package com.cyberx.web.db;

import com.cyberx.web.util.ApplicationProperties;

import java.sql.*;

public class DBConnection {

    private static Connection connection;

    public static Connection getConnection() throws Exception {
        if (connection == null) {

            ApplicationProperties properties = ApplicationProperties.getInstance();

            Class.forName(properties.get("sql.connection.driver"));

            connection = DriverManager.getConnection(properties.get("sql.connection.url"), properties.get("sql.connection.username"), properties.get("sql.connection.password"));
        }

        return connection;
    }



}
