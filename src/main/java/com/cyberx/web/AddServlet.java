package com.cyberx.web;

import com.cyberx.web.model.User;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "add", urlPatterns = "/add")
public class AddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");

        if (name.isBlank()) {
            System.out.println("Name required");
            resp.sendRedirect("list");
        } else if (email.isBlank()) {
            System.out.println("Email required");
            resp.sendRedirect("list");
        } else {
            User user = new User(name, email);
            try {
                user.addUser();
                resp.sendRedirect("list");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}