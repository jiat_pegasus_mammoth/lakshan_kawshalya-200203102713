package com.cyberx.web;

import com.cyberx.web.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "list", urlPatterns = "/list")
public class ListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<User> users = User.getAllUsers();
            req.setAttribute("users",users);
            req.getRequestDispatcher("list.jsp").forward(req, resp);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
