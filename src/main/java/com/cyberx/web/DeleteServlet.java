package com.cyberx.web;

import com.cyberx.web.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "delete", urlPatterns = "/delete")
public class DeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            User.deleteUser(Integer.parseInt(req.getParameter("id")));
            resp.sendRedirect("list");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
